using AspDotNetLab2;
using System.Text;
using System.Text.Json;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddTransient<ITimerService, TimerService>();
builder.Services.AddScoped<IRandomService, RandomService>();
builder.Services.AddSingleton<IGeneralCounterService, GeneralCounterService>();

var app = builder.Build();

app.UseMiddleware<RandomServiceMiddleware>();
app.UseMiddleware<TimerServiceMiddleware>();

app.UseMiddleware<GeneralCounterMiddleware>();

#region 1. виведення інформації про всі сервіси
var services = builder.Services;
app.Map("services/list", async context =>
{
    var sb = new StringBuilder();
    foreach (var serviceDescriptor in services)
    {
        sb.AppendLine($"Service Type: {serviceDescriptor.ServiceType.FullName}");
        sb.AppendLine($"Implementation Type: {serviceDescriptor.ImplementationType?.FullName ?? "None"}");
        sb.AppendLine($"Lifetime: {serviceDescriptor.Lifetime}");
        sb.AppendLine();
    }
    context.Response.ContentType = "text/plain";
    await context.Response.WriteAsync($"Count: {services.Count}\n{sb.ToString()}");
});
#endregion 

#region 2.1 Тип “Transient”, назва “TimerService” - повертає поточну дату та час.
app.Map("services/timer", async context =>
{
    var timerService = app.Services.GetRequiredService<ITimerService>();
    var dateTime = timerService.GetCurrentDateTime();
    await context.Response.WriteAsync(dateTime);
});
#endregion

#region 2.2 Тип “Scoped”, назва “RandomService” - в конструкторі генерує випадкове число.
app.Map("services/random", async (context) =>
{
    var randomService1 = context.RequestServices.GetRequiredService<IRandomService>();
    var randomService2 = context.RequestServices.GetRequiredService<IRandomService>();
    await context.Response.WriteAsync($"{randomService1.Value.ToString()} = {randomService2.Value.ToString()}");
});
#endregion

#region 2.3 Тип “Singleton”, назва “GeneralCounterService”
app.Map("services/general-counter", async context =>
{
    var counter = app.Services.GetRequiredService<IGeneralCounterService>();
    
    await context.Response.WriteAsync(JsonSerializer.Serialize(counter.GetCounts()));
});
#endregion
app.Map("/", async context => {
    var str = "";
    foreach (KeyValuePair<object, object?> kpv in context.Items) {
        str = $"{kpv.Key as string}: {kpv.Value as string ?? string.Empty}\n";
    }
    await context.Response.WriteAsync(str);
});
app.Run();
