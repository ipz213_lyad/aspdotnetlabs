﻿namespace AspDotNetLab2
{
    public interface ITimerService
    {
        string GetCurrentDateTime();
    }

    public class TimerService : ITimerService
    {
        public string GetCurrentDateTime()
        {
            return DateTime.Now.ToString();
        }
    }

}
