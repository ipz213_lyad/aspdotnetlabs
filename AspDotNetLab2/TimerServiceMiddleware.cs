﻿namespace AspDotNetLab2
{
    public class TimerServiceMiddleware
    {
        private readonly RequestDelegate _next;

        public TimerServiceMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ITimerService timerService)
        {
            var dateTime = timerService.GetCurrentDateTime();

              context.Items["TimerServiceMiddleware"] = dateTime;

            await _next(context);
        }
    }
}
