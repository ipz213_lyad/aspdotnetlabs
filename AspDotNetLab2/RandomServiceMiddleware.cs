﻿namespace AspDotNetLab2
{
    public class RandomServiceMiddleware
    {
        private readonly RequestDelegate _next;

        public RandomServiceMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, IRandomService randomService)
        {
            var randomNumber = randomService.Value;

            context.Items["RandomServiceMiddleware"] = randomNumber;

            await _next(context);
        }
    }
}
