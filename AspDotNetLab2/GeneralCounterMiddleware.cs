﻿namespace AspDotNetLab2
{
    public class GeneralCounterMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IGeneralCounterService _generalCounterService;

        public GeneralCounterMiddleware(RequestDelegate next, IGeneralCounterService generalCounterService)
        {
            _next = next;
            _generalCounterService = generalCounterService;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            string url = context.Request.Path;
            _generalCounterService.IncrementCounter(url);
            
            await _next(context);
        }
    }

}
