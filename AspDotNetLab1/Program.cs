
using Microsoft.Extensions.FileProviders;
using static System.Net.Mime.MediaTypeNames;

var builder = WebApplication.CreateBuilder(args);
var app = builder.Build();

// app.Environment.EnvironmentName = "Production";
#region 4. Реалізація обробки помилок
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler(
        app =>
            app.Run(async context =>
            {
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                context.Response.ContentType = Text.Plain;
                await context.Response.WriteAsync("Something went wrong...");
            })
    );
}
app.UseStatusCodePages(async statusCodeContext =>
{
    var response = statusCodeContext.HttpContext.Response;

    if (response.StatusCode == 404)
    {
        response.ContentType = "text/html;";
        await response.SendFileAsync("wwwroot/error/404.html");
    }
    else
    {
        response.ContentType = "text/plain; charset=UTF-8";
        await response.WriteAsync($"Error {response.StatusCode}");
    }
});
#endregion

app.UseMiddleware<CustomMiddleware.LoggerMiddleware>();
app.MapGet("/", () => "Hello World!");
#region 3. Реалізація статичного сервера
app.UseFileServer();
app.UseFileServer(new FileServerOptions
{
    EnableDirectoryBrowsing = true,
    FileProvider = new PhysicalFileProvider(
Path.Combine(Directory.GetCurrentDirectory(), @"static"))
});
#endregion

app.UseMiddleware<CustomMiddleware.SecretMiddleware>(new List<string>{"/secret-1", "/secret-2"});

#region 5. Обробка маршрутів
app.Map(
    "/home",
    appBuilder =>
    {
        appBuilder.Map(
            "/index",
            appBuilder =>
            {
                appBuilder.Run(async context => await context.Response.WriteAsync("Index Page"));
            }
        );
        appBuilder.Map(
            "/about",
            appBuilder =>
            {
                appBuilder.Run(async context => await context.Response.WriteAsync("About Page"));
            }
        );
        
    }
);
#endregion

app.Run();











// //using Microsoft.Extensions.FileProviders;
// //var builder = WebApplication.CreateBuilder(args);
// //var app = builder.Build();
// //app.Environment.EnvironmentName = "Production";

// //app.UseExceptionHandler(exceptionHandlerApp
// //    => exceptionHandlerApp.Run(async context
// //        => await Results.Problem()
// //                     .ExecuteAsync(context)));

// //app.UseMiddleware<CustomMiddleware.LoggerMiddleware>();
// //app.UseMiddleware<CustomMiddleware.SecretMiddleware>();

// //app.Map("/exception", ()
// //    => { throw new InvalidOperationException("Sample Exception"); });
// //app.Map("/home", appBuilder =>
// //{
// //    appBuilder.Map("/index", Index);
// //    appBuilder.Map("/about", About);

// //    appBuilder.Run(async (context) => await context.Response.WriteAsync("Home Page"));
// //});

// //app.MapGet("/", () => "Hello World!");

// //app.UseFileServer();
// //app.UseFileServer(
// //    new FileServerOptions
// //    {
// //        EnableDirectoryBrowsing = true,
// //        FileProvider = new PhysicalFileProvider(
// //            Path.Combine(Directory.GetCurrentDirectory(), @"static")
// //        ),
// //    }
// //);

// //app.Run();

// //void Index(IApplicationBuilder appBuilder)
// //{
// //    appBuilder.Run(async context => await context.Response.WriteAsync("Index Page"));
// //}
// //void About(IApplicationBuilder appBuilder)
// //{
// //    appBuilder.Run(async context => await context.Response.WriteAsync("About Page"));
// //}
