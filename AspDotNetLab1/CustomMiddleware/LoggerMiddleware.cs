namespace CustomMiddleware
{
    public class LoggerMiddleware
    {
        private readonly RequestDelegate next;
        private readonly string logPath;

        public LoggerMiddleware(RequestDelegate next, string logPath = "access.txt")
        {
            this.next = next;
            this.logPath = logPath;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            var r = context.Request;
            var logMessage = new System.Text.StringBuilder();
            foreach (var header in r.Headers)
            {
                logMessage.Append( $"{header.Key}: {header.Value}\n");
            }
            logMessage.ToString();
            File.AppendAllText(logPath, $"[{DateTime.Now}] {r.Method} {r.Path}\n{logMessage.ToString()}\n");
            await next.Invoke(context);
        }
    }
}
