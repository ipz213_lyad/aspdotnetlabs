namespace CustomMiddleware {

public class SecretMiddleware
{
    private readonly RequestDelegate _next;
        private readonly List<string> _secretNumbers;
        public SecretMiddleware(RequestDelegate next, List<string> secretNumbers)
    {
        _next = next;
            _secretNumbers = secretNumbers;
        }

    public async Task InvokeAsync(HttpContext context)
    {
        if (_secretNumbers.Contains(context.Request.Path))
        {
                context.Response.ContentType = "text/plain; charset=utf-8";
                await context.Response.WriteAsync("Це секретний маршрут!");
        }
        else
        {
            await _next(context);
        }
    }
}

}
